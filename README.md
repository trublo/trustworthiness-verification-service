
Trustworthiness Service base URL: https://verifymedia-staging.herokuapp.com/

Collection ID: 605ddf3c30e10750724c8988

For polling you can use the GET endpoints and check the status of the item from the field checklistProperties.status.
All endpoints require authentication. Use given JWT token in request headers like so:

Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvYXV0aF90b2tlbiI6IjEyNjEyNzIxNzAwMTk0MDE3MjgtQjVSenVZNmtYMDBWazZtZ29nVXVudWtocHhFSFR3IiwibmFtZSI6InZpYTIwMjAiLCJhbGlhcyI6InZpYTIwMjAxIiwiaWQiOiIxMjYxMjcyMTcwMDE5NDAxNzI4IiwiYXZhdGFyIjoiaHR0cHM6Ly9hYnMudHdpbWcuY29tL3N0aWNreS9kZWZhdWx0X3Byb2ZpbGVfaW1hZ2VzL2RlZmF1bHRfcHJvZmlsZV9ub3JtYWwucG5nIiwib2F1dGhfdG9rZW5fc2VjcmV0IjoiRGdDeU9hT2RDbzJkdGlYMXpMUVp6OWxHRWRsdjFibE56bmxjRHRFOWtMeGcwIn0.-FBi3m64UM9PG4WpxuMCUHLCc0RI3Nu82Y9SbDd97vE



# Retrieve collection items
GET /collections/{collectionId}/items

# Retrieve specific collection item
GET /collections/{collectionId}/items/{itemId}

# Add item to collection (by url)
POST /collections/{collectionId}/items
body:
{
  collectionId: {collectionId},
  inputType: "url",
  value: {url}
}

# Delete item
DELETE /collections/{collectionId}/items/{itemId}

